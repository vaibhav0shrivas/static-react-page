import React, { Component } from "react";

class NewComponent extends Component {

    constructor(props) {
        super(props);

        this.state = {
            message: "Beep Beep Boop Boop",
            buttonContent: "Iam the Button",
            clicksOnButton: 0,
        };
    };

    styles = {
        fontStyle: "itaclic",
        color: "pink"
    };

    buttonClicker = () => {
        console.log("Button was clicked");
        this.setState({
            clicksOnButton: this.state.clicksOnButton + 1,
        });


    };

    render() {
        return (
            <div className="new-componet">
                <h1 style={this.styles}>I'am Heading of NewComponent</h1>
                <h3>{this.state.message}</h3>
                <div>{this.state.clicksOnButton}</div>
                <button onClick={this.buttonClicker}>{this.state.buttonContent}</button>
            </div>
        );

    };

}

export default NewComponent;
import logo from './logo.svg';
import React from 'react';
import './App.css';
import NewComponent from './Components/NewComponent';


class App extends React.Component {

  styles = {
    fontStyle: "bold",
    color: "teal"

  };

  render() {
    return (
      <div className="App" >
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 style={this.styles}>React</h1>
          <NewComponent/>
          <p>
            Edit <code>src/App.js</code> and save to reload.
          </p>
          <a
            className="App-link"
            href="https://reactjs.org"
            target="_blank"
            rel="noopener noreferrer"
          >
            Learn React
          </a>
        </header>
      </div>
    );
  };
}

export default App;
